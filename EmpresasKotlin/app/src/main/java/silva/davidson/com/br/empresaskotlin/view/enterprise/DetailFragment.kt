package silva.davidson.com.br.empresaskotlin.view.enterprise

import android.os.Bundle
import android.view.*
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import silva.davidson.com.br.empresaskotlin.R
import silva.davidson.com.br.empresaskotlin.api.PHOTO_URL
import silva.davidson.com.br.empresaskotlin.databinding.DetailFragmentBinding
import silva.davidson.com.br.empresaskotlin.injector.GlideApp

class DetailFragment: Fragment() {

    private val args by navArgs<DetailFragmentArgs>()

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.menu, menu)

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        (activity as AppCompatActivity).supportActionBar?.apply {
            activity?.findViewById<ImageView>(R.id.logo_toolbar)?.visibility = View.VISIBLE
            show()
        }

        setHasOptionsMenu(false)

        return DetailFragmentBinding.inflate(inflater, container, false).let {
            it.enterprise = args.enterprise

                    GlideApp.with(this)
                        .load(PHOTO_URL + it.enterprise?.photo)
                        .placeholder(R.drawable.img_e_1)
                        .error(R.drawable.img_e_1)
                        //.transform(BlurTransformation()) Decidí por não usar, não ficou bom :
                        .into(it.enterpriseImage)

            it.root
        }
    }
}