package silva.davidson.com.br.empresaskotlin.viewModel.factory

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import silva.davidson.com.br.empresaskotlin.repository.InvestorRepository
import silva.davidson.com.br.empresaskotlin.viewModel.login.LoginViewModel

@Suppress("UNCHECKED_CAST")
class LoginViewModelFactory(private val repository:InvestorRepository):ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass:Class<T>) =
            LoginViewModel(repository) as T

}