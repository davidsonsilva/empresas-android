package silva.davidson.com.br.empresaskotlin.view.home

import android.content.Context
import android.os.Bundle
import android.view.*
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance
import silva.davidson.com.br.empresaskotlin.R
import silva.davidson.com.br.empresaskotlin.adapter.EnterpriseRecyclerViewAdapter
import silva.davidson.com.br.empresaskotlin.data.enterprise.Enterprise
import silva.davidson.com.br.empresaskotlin.data.enterprise.EnterpriseList
import silva.davidson.com.br.empresaskotlin.data.enterprise.EnterpriseResponse
import silva.davidson.com.br.empresaskotlin.databinding.HomeFragmentBinding
import silva.davidson.com.br.empresaskotlin.viewModel.factory.HomeViewModelFactory
import silva.davidson.com.br.empresaskotlin.viewModel.home.HomeViewModel

class HomeFragment : Fragment(), KodeinAware {

    override val kodein: Kodein by kodein()

    private val viewModelFactory: HomeViewModelFactory by instance()

    private val viewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory)
            .get(HomeViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        activity?.window?.statusBarColor = ContextCompat.getColor(context as Context, R.color.dark_pink)

        (activity as AppCompatActivity).supportActionBar?.apply {
            activity?.findViewById<ImageView>(R.id.logo_toolbar)?.visibility = View.VISIBLE
            title = ""
            show()
        }

        setHasOptionsMenu(true)

        return HomeFragmentBinding.inflate(inflater, container, false).let {
            it.adapter = EnterpriseRecyclerViewAdapter(
                mutableListOf(),
                object : EnterpriseRecyclerViewAdapter.OnItemClickListener {
                    override fun onItemClick(enterprise: Enterprise) {
                        findNavController().navigate(HomeFragmentDirections.actionShow(enterprise))
                    }
                })

            it.setLifecycleOwner(this)

            viewModel.enterprises.observe(this, Observer<EnterpriseList> { data ->
                it.adapter?.setData(data.enterprises)
            })

            viewModel.enterprise.observe(this, Observer<EnterpriseResponse> { data ->
                if(data.success) {
                    val list: ArrayList<Enterprise> = ArrayList()
                    list.add(data.enterprise)
                    it.adapter?.setData(list)
                }
                //it.adapter?.setData(data.enterprises)
            })

            it.root
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.menu, menu)

        val searchView = menu.findItem(R.id.enterprise_search).actionView  as SearchView
        searchView.setQueryHint(getString(R.string.seart_hint_text))

        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {

            override fun onQueryTextChange(newText: String?) = true

            override fun onQueryTextSubmit(query: String) =  searchAlternative(query)

                /*CoroutineScope(Dispatchers.Main).launch { viewModel.searchByName(query,
                args.header ) }.run { true }*/
        })
    }

    //Um teste que vou fazer para distinguir a pesquisa do usuário
    fun searchAlternative(query : String) : Boolean {
        val numeric = query.matches("-?\\d+(\\.\\d+)?".toRegex())
        if (numeric) {
            CoroutineScope(Dispatchers.Main).launch {
                viewModel.searchById(
                    query.toInt()
                )
            }.run { true }
        } else if (query.equals("*")) {
            CoroutineScope(Dispatchers.Main).launch {
                viewModel.showAll() }.run { true }
            }  else {
                    CoroutineScope(Dispatchers.Main).launch {
                        viewModel.searchByName(
                            query
                            //args.header
                        )
                    }.run { true }
        }
        return false
    }
}

