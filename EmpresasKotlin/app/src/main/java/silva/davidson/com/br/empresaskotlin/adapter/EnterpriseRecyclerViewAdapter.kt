package silva.davidson.com.br.empresaskotlin.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import silva.davidson.com.br.empresaskotlin.R
import silva.davidson.com.br.empresaskotlin.api.PHOTO_URL
import silva.davidson.com.br.empresaskotlin.data.enterprise.Enterprise
import silva.davidson.com.br.empresaskotlin.databinding.EnterpriseListItemBinding
import silva.davidson.com.br.empresaskotlin.injector.GlideApp

class EnterpriseRecyclerViewAdapter(private val enterprises: MutableList<Enterprise>,
                         private val onItemClickListener: OnItemClickListener)

    : RecyclerView.Adapter<EnterpriseRecyclerViewAdapter.EnterpriseViewHolder>() {

    class EnterpriseViewHolder(private val binding: EnterpriseListItemBinding)
        : RecyclerView.ViewHolder(binding.root) {

        fun bind(enterprise: Enterprise, onItemClickListener: OnItemClickListener) {

            binding.enterprise = enterprise
            binding.executePendingBindings()
            itemView.setOnClickListener { onItemClickListener.onItemClick(enterprise) }

            GlideApp.with(itemView)
                    .load(PHOTO_URL + enterprise.photo)
                    .placeholder(R.drawable.img_e_1)
                    .error(R.drawable.img_e_1)
                    .into(binding.enterpriseLogo)

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        EnterpriseViewHolder(EnterpriseListItemBinding.inflate(
            LayoutInflater.from(parent.context), parent, false)
        )

    override fun getItemCount() = enterprises.size

    override fun onBindViewHolder(holder: EnterpriseViewHolder, position: Int) {
        holder.bind(enterprises[position], onItemClickListener)
    }

    fun setData(list: List<Enterprise>) {
        enterprises.apply {
            clear()
            addAll(list)
        }

        notifyDataSetChanged()
    }


    interface OnItemClickListener {
        fun onItemClick(enterprise: Enterprise)
    }
}