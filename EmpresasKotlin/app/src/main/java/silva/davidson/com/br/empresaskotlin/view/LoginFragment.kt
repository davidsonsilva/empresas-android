package silva.davidson.com.br.empresaskotlin.view


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.direct
import org.kodein.di.generic.instance
import silva.davidson.com.br.empresaskotlin.R
import silva.davidson.com.br.empresaskotlin.data.user.Login
import silva.davidson.com.br.empresaskotlin.databinding.LoginFragmentBinding
import silva.davidson.com.br.empresaskotlin.viewModel.factory.LoginViewModelFactory
import silva.davidson.com.br.empresaskotlin.viewModel.login.LoginViewModel

class LoginFragment : Fragment(), KodeinAware   {

    override val kodein: Kodein by kodein()

    private val viewModelFactory: LoginViewModelFactory by instance()

    private val viewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory)
                .get(LoginViewModel::class.java)
    }

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {

        (activity as AppCompatActivity).supportActionBar?.hide()

        return LoginFragmentBinding.inflate(inflater, container, false).run {
            login.setOnClickListener { CoroutineScope(Dispatchers.Main).launch {
                it.isEnabled = false
                val succsses =  viewModel.auth(Login(email = email, password = password))
                if (succsses) {
                            findNavController().navigate(R.id.home_fragment)
                            Toast.makeText(activity, getString(R.string.login_sucesso), Toast.LENGTH_LONG).show()
                                .apply { it.isEnabled = false }
                }else {
                            Toast.makeText(activity, getString(R.string.erro_login), Toast.LENGTH_LONG).show()
                            .apply { it.isEnabled = true }
                      }
                }
            }
            root
        }
     }

}

inline fun <reified VM : ViewModel, T> T.viewModel(): Lazy<VM> where T : KodeinAware, T : FragmentActivity {
        return lazy { ViewModelProviders.of(this, direct.instance()).get(VM::class.java) }
}

