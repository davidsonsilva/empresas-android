package silva.davidson.com.br.empresaskotlin.data.enterprise

data class EnterpriseResponse(
    val enterprise : Enterprise = Enterprise(),
    val success : Boolean = false
)