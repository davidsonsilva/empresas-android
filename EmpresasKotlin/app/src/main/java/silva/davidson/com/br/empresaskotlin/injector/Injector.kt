package silva.davidson.com.br.empresaskotlin.injector


import silva.davidson.com.br.empresaskotlin.repository.InvestorRepository
import silva.davidson.com.br.empresaskotlin.repository.enterprise.EnterpriseRepository
import silva.davidson.com.br.empresaskotlin.viewModel.factory.HomeViewModelFactory
import silva.davidson.com.br.empresaskotlin.viewModel.factory.LoginViewModelFactory

object Injector {
    fun provideLoginViewModelFactory() = LoginViewModelFactory(InvestorRepository)

    fun provideHomeViewModelFactory() = HomeViewModelFactory(EnterpriseRepository)
}