package silva.davidson.com.br.empresaskotlin.data.enterprise

import android.os.Parcel
import android.os.Parcelable


data class EnterprisePojo (val name:String? = null,
                           val description: String? = null,
                           val photo: String? = null) : Parcelable{

    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString(),
        parcel.readString()
    )


    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(name)
        parcel.writeString(description)
        parcel.writeString(photo)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<EnterprisePojo> {
        override fun createFromParcel(parcel: Parcel): EnterprisePojo {
            return EnterprisePojo(parcel)
        }

        override fun newArray(size: Int): Array<EnterprisePojo?> {
            return arrayOfNulls(size)
        }
    }


}