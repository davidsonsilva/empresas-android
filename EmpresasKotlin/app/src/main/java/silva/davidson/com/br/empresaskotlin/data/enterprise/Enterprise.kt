package silva.davidson.com.br.empresaskotlin.data.enterprise

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class Enterprise(
    val id: Int = 0,
    val description: String = "",
    val city: String = "",
    val country: String = "",
    val photo: String = "",
    @SerializedName("enterprise_name")
    val name: String = "",
    @SerializedName("enterprise_type")
    val type: EnterpriseType = EnterpriseType()
):Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readInt(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readParcelable(EnterpriseType::class.java.classLoader)
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id)
        parcel.writeString(description)
        parcel.writeString(city)
        parcel.writeString(country)
        parcel.writeString(photo)
        parcel.writeString(name)
        parcel.writeParcelable(type, flags)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Enterprise> {
        override fun createFromParcel(parcel: Parcel): Enterprise {
            return Enterprise(parcel)
        }

        override fun newArray(size: Int): Array<Enterprise?> {
            return arrayOfNulls(size)
        }
    }
}
