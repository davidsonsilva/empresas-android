package silva.davidson.com.br.empresaskotlin.data.user

data class Login (
    val id:Int? = null,
    val email:String? = null,
    val password:String? =null
)