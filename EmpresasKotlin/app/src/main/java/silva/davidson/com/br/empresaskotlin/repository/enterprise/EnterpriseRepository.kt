package silva.davidson.com.br.empresaskotlin.repository.enterprise

import android.util.Log
import silva.davidson.com.br.empresaskotlin.api.EnterpriseService
import silva.davidson.com.br.empresaskotlin.data.enterprise.EnterpriseList
import silva.davidson.com.br.empresaskotlin.data.enterprise.EnterpriseResponse

object EnterpriseRepository {

    suspend fun searchByName(name: String?): EnterpriseList =
            EnterpriseService.invoke().searchByNameAsync(name)
                .await().let {
                    it
            }

    suspend fun searchById(id: Int): EnterpriseResponse {
        try {
           val enterpriseResponse = EnterpriseService.invoke()
                .searchByIdAsync(id).await()
            if (enterpriseResponse.success) {
                return enterpriseResponse
            }

        }catch (e: retrofit2.HttpException){
            Log.e("searchById", e.message())
        }
        return EnterpriseResponse()
    }

    suspend fun showAll(): EnterpriseList
            = EnterpriseService.invoke().showAllAsync().await().let { it }

}