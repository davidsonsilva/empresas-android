package silva.davidson.com.br.empresaskotlin.api

import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import kotlinx.coroutines.Deferred
import okhttp3.OkHttpClient
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*
import silva.davidson.com.br.empresaskotlin.auth.AuthResponse
import silva.davidson.com.br.empresaskotlin.data.enterprise.EnterpriseList
import silva.davidson.com.br.empresaskotlin.data.enterprise.EnterpriseResponse
import silva.davidson.com.br.empresaskotlin.data.user.Login

const val BASE_URL = "http://empresas.ioasys.com.br/api/v1/"
const val PHOTO_URL = "http://empresas.ioasys.com.br/"
const val BASE_PATH_LOGIN = "users/auth/sign_in/"
const val BASE_PATH_ENTERPRISES = "enterprises/"
const val BASE_PATH_ENTERPRISES_ID ="enterprises/{id}"

interface EnterpriseService {

    @POST(BASE_PATH_LOGIN)
    fun authAsync(@Body login: Login): Deferred<Response<AuthResponse>>


    @GET(BASE_PATH_ENTERPRISES)
    fun showAllAsync(): Deferred<EnterpriseList>

    @GET(BASE_PATH_ENTERPRISES_ID)
    fun searchByIdAsync(@Path("id") id: Int): Deferred<EnterpriseResponse>

    @GET(BASE_PATH_ENTERPRISES)
    fun searchByNameAsync(@Query("name") name: String?): Deferred<EnterpriseList>

    companion object {
        operator fun invoke():EnterpriseService {
                return Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .client(OkHttpClient.Builder().addInterceptor(EnterpriseInterceptor).build())
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(CoroutineCallAdapterFactory())
                    .build()
                    .create(EnterpriseService::class.java)
        }
    }

}