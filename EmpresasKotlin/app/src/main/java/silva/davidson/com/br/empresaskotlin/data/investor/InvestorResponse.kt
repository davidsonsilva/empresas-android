package silva.davidson.com.br.empresaskotlin.data.investor

import com.google.gson.annotations.SerializedName

data class InvestorResponse(
    @SerializedName("enterprise")
    val enterprise: Any,
    @SerializedName("investor")
    val investor: Investor,
    @SerializedName("success")
    val success: Boolean
)