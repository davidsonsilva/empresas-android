package silva.davidson.com.br.empresaskotlin.viewModel.home

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import silva.davidson.com.br.empresaskotlin.api.EnterpriseInterceptor
import silva.davidson.com.br.empresaskotlin.data.enterprise.EnterpriseList
import silva.davidson.com.br.empresaskotlin.data.enterprise.EnterpriseResponse
import silva.davidson.com.br.empresaskotlin.repository.enterprise.EnterpriseRepository

class HomeViewModel(private val repository: EnterpriseRepository) : ViewModel() {

    val enterprises by lazy { MutableLiveData<EnterpriseList>() }

    val enterprise by lazy { MutableLiveData<EnterpriseResponse>() }

    suspend fun searchByName(name:String) { enterprises.postValue(repository.searchByName(name)) }

    suspend fun showAll() { enterprises.postValue(repository.showAll()) }

    suspend fun searchById(id:Int) { enterprise.postValue(repository.searchById(id)) }

}