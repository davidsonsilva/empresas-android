package silva.davidson.com.br.empresaskotlin.data.enterprise

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class EnterpriseType(
    @SerializedName("enterprise_type_name")
    val enterpriseTypeName: String = "",
    @SerializedName("id")
    val id: Int = 0
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readInt()
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(enterpriseTypeName)
        parcel.writeInt(id)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<EnterpriseType> {
        override fun createFromParcel(parcel: Parcel): EnterpriseType {
            return EnterpriseType(parcel)
        }

        override fun newArray(size: Int): Array<EnterpriseType?> {
            return arrayOfNulls(size)
        }
    }
}