package silva.davidson.com.br.empresaskotlin.viewModel.login

import androidx.lifecycle.ViewModel
import silva.davidson.com.br.empresaskotlin.data.user.Login
import silva.davidson.com.br.empresaskotlin.repository.InvestorRepository

class LoginViewModel(private val repository:InvestorRepository): ViewModel() {

    suspend fun auth(login: Login) = repository.auth(login).success

}