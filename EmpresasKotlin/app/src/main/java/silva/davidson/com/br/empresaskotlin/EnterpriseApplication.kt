package silva.davidson.com.br.empresaskotlin

import android.app.Application
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.androidXModule
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.singleton
import silva.davidson.com.br.empresaskotlin.repository.InvestorRepository
import silva.davidson.com.br.empresaskotlin.repository.enterprise.EnterpriseRepository
import silva.davidson.com.br.empresaskotlin.viewModel.factory.HomeViewModelFactory
import silva.davidson.com.br.empresaskotlin.viewModel.factory.LoginViewModelFactory

open class EnterpriseApplication: Application(), KodeinAware {

    override val kodein = Kodein.lazy {
        import(androidXModule(this@EnterpriseApplication))
        bind<InvestorRepository>("investorRepository") with singleton { InvestorRepository }
        bind <EnterpriseRepository>("enterpriseRepository") with singleton { EnterpriseRepository }
        bind<LoginViewModelFactory>() with singleton { LoginViewModelFactory(instance("investorRepository")) }
        bind<HomeViewModelFactory>() with singleton { HomeViewModelFactory(instance("enterpriseRepository")) }
    }

    companion object Constants {
        const val TAG = "myAmazingProject"
    }

    override fun onCreate() {
        super.onCreate()
        ObjectBox.init(this);
    }
}