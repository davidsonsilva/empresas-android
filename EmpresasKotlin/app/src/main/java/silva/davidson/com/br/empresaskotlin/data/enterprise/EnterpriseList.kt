package silva.davidson.com.br.empresaskotlin.data.enterprise

data class EnterpriseList(
    val enterprises: List<Enterprise>
)