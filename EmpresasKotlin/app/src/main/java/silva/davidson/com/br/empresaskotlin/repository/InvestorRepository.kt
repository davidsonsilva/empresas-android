package silva.davidson.com.br.empresaskotlin.repository

import silva.davidson.com.br.empresaskotlin.api.EnterpriseInterceptor
import silva.davidson.com.br.empresaskotlin.api.EnterpriseService
import silva.davidson.com.br.empresaskotlin.auth.AuthResponse
import silva.davidson.com.br.empresaskotlin.data.user.Login

object InvestorRepository {

    val enterpriseInterceptor by lazy { EnterpriseInterceptor }

    suspend fun auth(login: Login): AuthResponse =
        EnterpriseService.invoke().authAsync(login).await().let {
            if (it.isSuccessful) {

                EnterpriseInterceptor.accessToken = it.headers().get("access-token")
                EnterpriseInterceptor.client = it.headers().get("client")
                EnterpriseInterceptor.uid = it.headers().get("uid")

                //enterpriseInterceptor.accessToken = it.headers().get("access-token")
                //enterpriseInterceptor.client = it.headers().get("client")
                //enterpriseInterceptor.uid = it.headers().get("uid")
            }
            it.body() ?: AuthResponse()
        }
}