package silva.davidson.com.br.empresaskotlin.auth

import silva.davidson.com.br.empresaskotlin.data.enterprise.Enterprise
import silva.davidson.com.br.empresaskotlin.data.investor.InvestorResponse

data class AuthResponse(
    val investor: InvestorResponse? = null,
    val enterprise: Enterprise? = null,
    val success: Boolean = false
)