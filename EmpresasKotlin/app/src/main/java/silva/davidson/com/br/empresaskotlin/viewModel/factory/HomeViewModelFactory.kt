package silva.davidson.com.br.empresaskotlin.viewModel.factory

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import silva.davidson.com.br.empresaskotlin.repository.enterprise.EnterpriseRepository
import silva.davidson.com.br.empresaskotlin.viewModel.home.HomeViewModel

@Suppress("UNCHECKED_CAST")
class  HomeViewModelFactory(private val repository: EnterpriseRepository)
    : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>) =
         HomeViewModel(repository) as T

}