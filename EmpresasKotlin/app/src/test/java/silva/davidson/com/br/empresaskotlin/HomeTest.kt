package silva.davidson.com.br.empresaskotlin

import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertNotNull
import org.junit.Before
import org.junit.Test
import silva.davidson.com.br.empresaskotlin.auth.AuthResponse
import silva.davidson.com.br.empresaskotlin.data.enterprise.Enterprise
import silva.davidson.com.br.empresaskotlin.data.enterprise.EnterpriseList
import silva.davidson.com.br.empresaskotlin.data.user.Login
import silva.davidson.com.br.empresaskotlin.injector.Injector
import silva.davidson.com.br.empresaskotlin.viewModel.factory.HomeViewModelFactory
import silva.davidson.com.br.empresaskotlin.viewModel.factory.LoginViewModelFactory
import silva.davidson.com.br.empresaskotlin.viewModel.home.HomeViewModel
import silva.davidson.com.br.empresaskotlin.viewModel.login.LoginViewModel

class HomeTest {

    lateinit var user : Login
    lateinit var loginFactor : LoginViewModelFactory
    lateinit var loginViewModel: LoginViewModel

    lateinit var homeViewModelFactory: HomeViewModelFactory
    lateinit var homeViewModel: HomeViewModel


    @Before
    fun setup() {
        //Fazendo o login e buscando os dados do Header
        this.loginFactor = Injector.provideLoginViewModelFactory()
        this.loginViewModel = loginFactor.create(LoginViewModel::class.java)
        this.user = Login(null, "testeapple@ioasys.com.br", "12341234")

        this.homeViewModelFactory = Injector.provideHomeViewModelFactory()
        this.homeViewModel = homeViewModelFactory.create(HomeViewModel::class.java)


    }

    @Test
    fun getEnterprises(){
        val result = runBlocking { loginViewModel.auth(user) }

        //val header = loginViewModel.investor()

        val resultList  = runBlocking { homeViewModel.showAll()}

        assertNotNull( resultList)

    }

}