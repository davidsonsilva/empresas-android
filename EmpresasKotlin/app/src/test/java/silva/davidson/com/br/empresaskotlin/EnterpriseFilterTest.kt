package silva.davidson.com.br.empresaskotlin


import android.util.Log
import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.*
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import silva.davidson.com.br.empresaskotlin.data.enterprise.EnterpriseList
import silva.davidson.com.br.empresaskotlin.data.user.Login
import silva.davidson.com.br.empresaskotlin.injector.Injector
import silva.davidson.com.br.empresaskotlin.viewModel.factory.HomeViewModelFactory
import silva.davidson.com.br.empresaskotlin.viewModel.factory.LoginViewModelFactory
import silva.davidson.com.br.empresaskotlin.viewModel.home.HomeViewModel
import silva.davidson.com.br.empresaskotlin.viewModel.login.LoginViewModel

class EnterpriseFilterTest {

    private lateinit var user: Login
    private lateinit var loginFactor: LoginViewModelFactory
    private lateinit var loginViewModel: LoginViewModel
    private lateinit var homeViewModelFactory: HomeViewModelFactory
    private lateinit var homeViewModel: HomeViewModel


    @Before
    fun setup() {
        //Fazendo o login e buscando os dados do Header
        this.loginFactor = Injector.provideLoginViewModelFactory()
        this.loginViewModel = loginFactor.create(LoginViewModel::class.java)
        this.user = Login(null, "testeapple@ioasys.com.br", "12341234")
        //Instanciando o HomeViewModel
        this.homeViewModelFactory = Injector.provideHomeViewModelFactory()
        this.homeViewModel = homeViewModelFactory.create(HomeViewModel::class.java)
    }

    @Test
    fun getEnterpriseByName(){
        //Efetuando o Login
        runBlocking { loginViewModel.auth(user) }
        //Consultando pelo nome AllRide
        val enterpriseFilter = runBlocking { homeViewModel.searchByName("AllRide")}
        assertNotNull(enterpriseFilter)
    }

    @Test
    fun getEnterpriseById(){
        //Efetuando o Login
        runBlocking { loginViewModel.auth(user) }
        //Consultando pelo ID = 1
        val enterpriseFilter = runBlocking { homeViewModel.searchById(1)}

        assertNotNull( enterpriseFilter)
    }

    @Test
    fun getEnterpriseByIdNotFound(){

        runBlocking { loginViewModel.auth(user)}

        val enterpriseFilter = runBlocking { homeViewModel.searchById(0)}

        assertEquals(kotlin.Unit, enterpriseFilter)

    }

}

