package silva.davidson.com.br.empresaskotlin

import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import silva.davidson.com.br.empresaskotlin.data.user.Login
import silva.davidson.com.br.empresaskotlin.injector.Injector
import silva.davidson.com.br.empresaskotlin.viewModel.factory.LoginViewModelFactory
import silva.davidson.com.br.empresaskotlin.viewModel.login.LoginViewModel

@RunWith(JUnit4::class)
class LoginTest {

    lateinit var user : Login
    lateinit var loginFactor : LoginViewModelFactory
    lateinit var loginViewModel: LoginViewModel

    @Before
    fun setup() {

        this.loginFactor = Injector.provideLoginViewModelFactory()
        this.loginViewModel = loginFactor.create(LoginViewModel::class.java)
        this.user = Login(null, "testeapple@ioasys.com.br", "12341234")
    }

    @Test
    fun fetchUserLogin(){

        val result = runBlocking { loginViewModel.auth(user) }
        val expect = true

        assertEquals(expect, result)
    }
}